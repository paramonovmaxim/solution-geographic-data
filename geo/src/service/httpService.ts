import {IHttpService, IGetRangeInterface,IPostInterface,IDeleteInterface } from "../interfaces/IHttpService";
import { TableModel, ItemModel,PaginationModel} from "./../models/models";
import axios from "axios";
import store from './../store';

export class HttpService implements IHttpService {
    private static _instance: HttpService;
    getRange: IGetRangeInterface;
    put:IPostInterface;
    post:IPostInterface;
    delete:IDeleteInterface;
    private constructor() {
        this.getRange = (url: string, page: number, pageSize:number)=>{
            const path = url + "/?page=" + page + "&pageSize=" + pageSize;
            axios({
                url: path,
                method: "get",
                headers: {
                  "Content-type": "application/json"
                }
              }).then((res) => {
                const datatable = new TableModel();
                const pagModel = new PaginationModel();
                pagModel.countPage = res.data.countPage;
                for (let i = 0; i < res.data.list.length; i++) {
                  const it = new ItemModel();
                  it.id = res.data.list[i].id;
                  it.name = res.data.list[i].name;
                  datatable.items.push(it);
                  console.log(res.data.list[i].name);
                }
                store.commit("savegrid", datatable);
                store.commit("savepagging", pagModel);
              })
              .catch((err) => {
                console.log("Error");
              });
        }
        this.post = (url: string, data:any)=>{
            axios({
                url: url,
                method: "post",
                data:JSON.stringify(data) ,
                headers: {
                  "Content-type": "application/json"
                }
              }).then((res) => {
                console.log("New Items Id :" + res.data);
              })
              .catch((err) => {
                console.log("Error");
              });
        }
        this.put = (url: string, data:any) => {
            axios({
                url: url + "/?id=" + data.id,
                method: "put",
                data:JSON.stringify(data) ,
                headers: {
                  "Content-type": "application/json"
                }
              }).then((res) => {
                
              })
              .catch((err) => {
                console.log("Error");
              });
        }
        this.delete = (url: string, id:string) => {
          axios({
              url: url + "/" + id,
              method: "delete"
            }).then((res) => {
              
            })
            .catch((err) => {
              console.log("Error");
            });
      }
    }
    static getInstance() {
        return this._instance || (this._instance = new this());
    }
}