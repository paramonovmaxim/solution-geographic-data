interface IHttpService{
    getRange:IGetRangeInterface;
    post:IPostInterface;
    put:IPostInterface
}


interface IGetRangeInterface {
    (url: string, page: number, pageSize:number): void;
}

interface IPostInterface {
    (url: string, data: any): void;
}

interface IDeleteInterface {
    (url: string, id: string): void;
}

export {IHttpService,IGetRangeInterface,IPostInterface,IDeleteInterface}