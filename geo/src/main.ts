import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
// import 'font-awesome/css/font-awesome.css'

// import BootstrapVue from 'bootstrap-vue'

import VueRx from 'vue-rx'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader

// Vue.use(BootstrapVue);
Vue.use(VueRx)
Vue.use(Vuetify)

Vue.config.productionTip = false

console.log ("VUE_APP_DEBUG :" + process.env.VUE_APP_DEBUG)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
