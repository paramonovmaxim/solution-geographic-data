import Vue from 'vue'
import Router from 'vue-router'
import Country from './views/Country.vue'
import Region from './views/Region.vue'
import City from './views/City.vue'

Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
      path: '/',
      name: 'home',
      component: Country
    },
    {
      path: '/countries/',
      name: 'countries',
      component: Country
    },
    {
      path: '/regions',
      name: 'regions',
      component: Region
    },
    {
      path: '/cities',
      name: 'cities',
      component: City
    }
  ]
})
