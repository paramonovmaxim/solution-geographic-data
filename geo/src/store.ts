import Vue from 'vue'
import Vuex from 'vuex'
import { TableModel,PaginationModel } from "./models/models";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    grid:new TableModel(),
    pagging: new PaginationModel()
  },
  getters:{
    getgrid(state){
      return state.grid;
    },
    getpagging(state){
      return state.pagging;
    }
  },
  mutations: {
    savegrid(state, m:TableModel){
      state.grid = m;
    },
    savepagging(state, m:PaginationModel){
      state.pagging = m;
    }
  },
  actions: {

  }
})
