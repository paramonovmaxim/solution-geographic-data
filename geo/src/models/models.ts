export enum Constants {
    urlCountry = "https://localhost:44376/api/api-country/countries",
    urlRegion = "https://localhost:44376/api/api-region/regions",
    urlCity = "https://localhost:44376/api/api-city/cities"
}
export class AppBuffer {
    static url: string;
    static router:string;
}
export class GridItem {
    public id!: number;
    public name!: string;
}
export class PageInfo {
    constructor() {
        this.totalItems = 0;
        this.countPage = 0;
        this.list = new Array<GridItem>();
    }
    public totalItems: number;
    public countPage: number;
    public list: Array<GridItem>;
}
export class TableModel {
    constructor() {
        this.items = new Array<ItemModel>();
        this.headers = new Array<HeaderItem>();
        let it = new HeaderItem();
        it.text = "Id";
        it.value = "id";
        it.align = "left";
        this.headers.push(it);
        let it1 = new HeaderItem();
        it1.text = "Name";
        it1.value = "id";
        it1.align = "center";
        this.headers.push(it1);
    }
    items: Array<ItemModel>;
    headers: Array<HeaderItem>;
}

export class PaginationModel{
    constructor(){
        this.countPage = 0;
        this.currentPage = 1;
        this.sizePage = 10;
    }
    public countPage: number;
    public currentPage: number;
    public sizePage: number;
}

export class HeaderItem {
    text!: string;
    align!: string;
    value!: string;
}

export class ItemModel {
    id!: number;
    name!: string;
}