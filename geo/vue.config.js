module.exports = {
  configureWebpack: {
    devtool: 'source-map',
    
    //  module: {
    //   rules: [
    //       {
    //         test: /\.html$/,
    //         exclude: /index.html/,
    //         use: [
    //           'vue-template-loader'
    //         ]
    //       }
    //   ] 
    // }
  },
  devServer: {
    open: process.platform === 'darwin',
    host: '0.0.0.0',
    port: 8080, // CHANGE YOUR PORT HERE!
    https: false,
    hotOnly: false,
  }
}