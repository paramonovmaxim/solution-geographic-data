using AutoMapper;
using Domain.Entities;
using WebUi.ViewModel;

namespace WebUi.Infrastructure
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CountryDto, Country>();
            CreateMap<RegionDto, Region>();
            CreateMap<CityDto, City>();
        }
    }
}
