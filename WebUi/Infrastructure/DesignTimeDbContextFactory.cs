using Domain.Concrete;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace WebUi.Infrastructure
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<EfDbContext>
  {
    public EfDbContext CreateDbContext(string[] args)
    {
      var builder = new DbContextOptionsBuilder<EfDbContext>();
      builder.UseSqlServer(Constants.DefaultConnection);
      return new EfDbContext(builder.Options);
    }
  }
}
