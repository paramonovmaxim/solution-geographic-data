using AutoMapper;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Rewrite;
using WebUi.Data;
using WebUi.Infrastructure;
using WebUi.Models;

namespace WebUi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
              options.UseSqlServer(Constants.DefaultConnection));

            services.AddScoped<ICountryRepository, EfCountryRepository>();
            services.AddScoped<IRegionRepository, EfRegionRepository>();
            services.AddScoped<ICityRepository, EfCityRepository>();

            //services.AddDefaultIdentity<IdentityUser>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddDefaultIdentity<IdentityUser>(options =>
            {
                // Lockout settings
                // Определяет, можно ли заблокировать нового пользователя.
                options.Lockout.AllowedForNewUsers = true;
                //Время, в течение которого пользователь блокируется при возникновении блокировки.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                //Количество неудачных попыток доступа до тех пор, пока пользователь не будет заблокирован, если блокировка включена.
                options.Lockout.MaxFailedAccessAttempts = 5;
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequiredUniqueChars = 2;
                options.Password.RequireLowercase = true;
                //options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
            })
                .AddDefaultTokenProviders()
                .AddRoles<IdentityRole>()
                .AddRoleManager<RoleManager<IdentityRole>>()
                .AddUserManager<UserManager<IdentityUser>>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
               


            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
                //cfg.AllowNullCollections = true;
            });
            services.AddMemoryCache();
            services.AddCors(options => options.AddPolicy("CorsPolicy",
              builder =>
              {
                  builder.AllowAnyMethod().AllowAnyHeader()
                    .WithOrigins("http://localhost:44336")
                    .AllowCredentials();
              }));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true, //allow unkown file types also to be served
                DefaultContentType =
                    "image/jpeg" //content type to returned if fileType is not known.
            });
            app.UseCookiePolicy();
            app.UseAuthentication();


            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

            //loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt"));

            //Task.Run(async () => { await CreateRoles(serviceProvider); }).Wait();
        }
        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            string[] roleNames = { "Admin", "User" };

            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist) await roleManager.CreateAsync(new IdentityRole(roleName));
                else return;
            }
            var poweruser = new ApplicationUser { UserName = "admin@admin.net", Email = "admin@admin.net" };
            var user = await userManager.FindByEmailAsync("admin@admin.net");
            if (user == null)
            {
                var createPowerUser = await userManager.CreateAsync(poweruser, "z4129U$$$");
                if (createPowerUser.Succeeded)
                {
                    await userManager.AddToRoleAsync(poweruser, "Admin");
                }
            }
        }
    }
}
