﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUi.Lib
{
    public static class WorkLib
    {
        public static int CountPage(int totalItems, int pageSize)
        {
            int pageCount;
            if (totalItems < pageSize) pageCount = 1;
            else
            {
                pageCount = totalItems / pageSize;
                var t = totalItems % pageSize;
                if(t > 0) pageCount ++;
            }
            return pageCount;
        }
    }
}
