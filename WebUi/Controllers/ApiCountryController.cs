﻿using Domain.Abstract;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using WebUi.Lib;
using WebUi.ViewModel;
using Mapper = AutoMapper.Mapper;

namespace WebUi.Controllers
{
    [ApiController]
    public class ApiCountryController : ControllerBase
    {
        private readonly ICountryRepository _repo;
        public ApiCountryController(ICountryRepository repoParam)
        {
            _repo = repoParam;
        }

        // GET: api/ApiCountry
        [HttpGet]
        [Route("api/api-country/countries")]
        public async Task<PageInfo<CountryDto>> GetRange(int page = 1, int pageSize = 500)
        {
            var (items, totalItems) = await _repo.CountryGetRange(page, pageSize);
            var model = new List<CountryDto>();
            foreach (var p in items)
            {
                model.Add(Mapper.Map<CountryDto>(p));
            }
            var pageInfo = new PageInfo<CountryDto>();
            var pageCount = WorkLib.CountPage(totalItems, pageSize);
            pageInfo.SetItems(totalItems, pageCount, model);
            return pageInfo;
        }

        // GET: api/ApiCountry/5
        [HttpGet]
        [Route("api/api-country/countries/{id}")]
        public async Task<CountryDto> Get(int id)
        {
            var t = await _repo.CountryGet(id);
            return Mapper.Map<CountryDto>(t);
        }

        // POST: api/ApiCountry
        [HttpPost]
        [Route("api/api-country/countries")]
        public async Task<int> Post([FromBody] CountryDto m)
        {
            var t = Mapper.Map<Country>(m);
            await _repo.CountryCreate(t);
            return t.Id;
        }

        // PUT: api/ApiCountry/5
        [HttpPut]
        [Route("api/api-country/countries")]
        public async Task Put(int id, [FromBody] CountryDto m)
        {
            var t = Mapper.Map<Country>(m);
            t.Id = id;
            await _repo.CountryUpdate(t);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("api/api-country/countries")]
        public async Task Delete(int id)
        {
            await _repo.CountryRemove(id);
        }
    }
}
