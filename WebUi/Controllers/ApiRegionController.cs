﻿using Domain.Abstract;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using WebUi.Lib;
using WebUi.ViewModel;
using Mapper = AutoMapper.Mapper;

namespace WebUi.Controllers
{
    [ApiController]
    public class ApiRegionController : ControllerBase
    {
        private readonly IRegionRepository _repo;
        public ApiRegionController(IRegionRepository repoParam)
        {
            _repo = repoParam;
        }

        // GET: api/ApiRegion
        [HttpGet]
        [Route("api/api-region/regions")]
        public async Task<PageInfo<RegionDto>> GetRange(int page = 1, int pageSize = 500)
        {
            var (items, totalItems) = await _repo.RegionGetRange(page, pageSize);
            var model = new List<RegionDto>();
            foreach (var p in items)
            {
                model.Add(Mapper.Map<RegionDto>(p));
            }
            var pageInfo = new PageInfo<RegionDto>();
            var pageCount = WorkLib.CountPage(totalItems, pageSize);
            pageInfo.SetItems(totalItems, pageCount, model);
            return pageInfo;
        }

        // GET: api/ApiRegion/5
        [HttpGet]
        [Route("api/api-region/regions/{id}")]
        public async Task<RegionDto> Get(int id)
        {
            var t = await _repo.RegionGet(id);
            return Mapper.Map<RegionDto>(t);
        }

        // POST: api/ApiRegion
        [HttpPost]
        [Route("api/api-region/regions")]
        public async Task<int> Post([FromBody] RegionDto m)
        {
            var t = Mapper.Map<Region>(m);
            await _repo.RegionCreate(t);
            return t.Id;
        }

        // PUT: api/ApiRegion/5
        [HttpPut]
        [Route("api/api-region/regions")]
        public async Task Put(int id, [FromBody] RegionDto m)
        {
            var t = Mapper.Map<Region>(m);
            t.Id = id;
            await _repo.RegionUpdate(t);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("api/api-region/regions")]
        public async Task Delete(int id)
        {
            await _repo.RegionRemove(id);
        }
    }
}
