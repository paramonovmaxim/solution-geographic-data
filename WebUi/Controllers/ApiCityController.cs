﻿using Domain.Abstract;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using WebUi.Lib;
using WebUi.ViewModel;
using Mapper = AutoMapper.Mapper;

namespace WebUi.Controllers
{
    [ApiController]
    public class ApiCityController : ControllerBase
    {
        private readonly ICityRepository _repo;
        public ApiCityController(ICityRepository repoParam)
        {
            _repo = repoParam;
        }

        // GET: api/ApiCity
        [HttpGet]
        [Route("api/api-city/cities")]
        public async Task<PageInfo<CityDto>> GetRange(int page = 1, int pageSize = 500)
        {
            var (items, totalItems) = await _repo.CityGetRange(page, pageSize);
            var model = new List<CityDto>();
            foreach (var p in items)
            {
                model.Add(Mapper.Map<CityDto>(p));
            }
            var pageInfo = new PageInfo<CityDto>();
            var pageCount = WorkLib.CountPage(totalItems, pageSize);
            pageInfo.SetItems(totalItems, pageCount, model);
            return pageInfo;
        }

        // GET: api/ApiCity/5
        [HttpGet]
        [Route("api/api-city/cities/{id}")]
        public async Task<CityDto> Get(int id)
        {
            var t = await _repo.CityGet(id);
            return Mapper.Map<CityDto>(t);
        }

        // POST: api/ApiCity
        [HttpPost]
        [Route("api/api-city/cities")]
        public async Task<int> Post([FromBody] CityDto m)
        {
            var t = Mapper.Map<City>(m);
            await _repo.CityCreate(t);
            return t.Id;
        }

        // PUT: api/ApiCity/5
        [HttpPut]
        [Route("api/api-city/cities")]
        public async Task Put(int id, [FromBody] CityDto m)
        {
            var t = Mapper.Map<City>(m);
            t.Id = id;
            await _repo.CityUpdate(t);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("api/api-city/cities")]
        public async Task Delete(int id)
        {
            await _repo.CityRemove(id);
        }
    }
}
