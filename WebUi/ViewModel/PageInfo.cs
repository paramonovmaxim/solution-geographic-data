﻿using System.Collections.Generic;
using Domain.Entities;

namespace WebUi.ViewModel
{
    public class PageInfo<T>
    {
        public void SetItems(int totalItems, int countPage, List<T> list)
        {
            TotalItems = totalItems;
            CountPage = countPage;
            List = list;
        }
        public int TotalItems { get; set; }
        public int CountPage { get; set; }
        public List<T> List { get; set; } = new List<T>();
    }
}
