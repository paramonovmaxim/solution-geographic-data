﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Region
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual Country Country { get; set; }
    }
}
