﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class City
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual Region Region { get; set; }
    }
}
