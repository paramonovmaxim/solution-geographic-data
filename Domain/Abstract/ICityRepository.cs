﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Models;


namespace Domain.Abstract
{
    public interface ICityRepository
    {
        IQueryable<City> Cities { get; }

        Task<City> CityGet(int id);
        Task<(List<City> items, int totalItems)> CityGetRange(int page, int pageSize);
        Task CityRemove(int id);
        Task CityUpdate(City c);
        Task CityCreate(City c);
    }
}
