﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Models;

namespace Domain.Abstract
{
    public interface IRegionRepository
    {
        IQueryable<Region> Regions { get; }
        Task<Region> RegionGet(int id);
        Task<(List<Region> items, int totalItems)> RegionGetRange(int page, int pageSize);
        Task RegionRemove(int id);
        Task RegionUpdate(Region c);
        Task RegionCreate(Region c);
    }
}
