﻿using System.Collections.Generic;
using Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface ICountryRepository
    {
        IQueryable<Country> Countries { get; }

        Task<Country> CountryGet(int id);
        Task<(List<Country> items, int totalItems)> CountryGetRange(int page, int pageSize);
        Task CountryRemove(int id);
        Task CountryUpdate(Country c);
        Task CountryCreate(Country c);
    }
}
