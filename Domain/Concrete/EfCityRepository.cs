﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Domain.Concrete
{
    public class EfCityRepository : EfBaseRepository, ICityRepository, IDisposable
    {
        public IQueryable<City> Cities => Context.Cities;

        public async Task CityCreate(City c)
        {
            if (c.Id != 0) return;
            Context.Cities.Add(c);
            await Context.SaveChangesAsync();
        }

        public async Task<City> CityGet(int id)
        {
            var q = Context.Cities.AsNoTracking()
                .Where(z => z.Id == id);
            var f = await q.AnyAsync();
            if (!f) return new City();
            return await q.SingleAsync();
        }

        public async Task<(List<City> items, int totalItems)> CityGetRange(int page, int pageSize)
        {
            var q = Context.Cities.AsNoTracking();
            var totalItems = q.Count();
            if (page > 1) q = q.Skip((page - 1) * pageSize);
            if (pageSize > 0) q = q.Take(pageSize);
            var f = await q.AnyAsync();
            if (!f) return (new List<City>(), 0);
            var list = await q.ToListAsync();
            return (list, totalItems);
        }

        public async Task CityUpdate(City c)
        {
            Context.Entry(c).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public async Task CityRemove(int id)
        {
            var q = Context.Cities.Where(z => z.Id == id);
            var f = await q.AnyAsync();
            if (!f) return;
            var c = await q.SingleAsync();
            Context.Cities.Remove(c);
            await Context.SaveChangesAsync();
        }
    }
}

