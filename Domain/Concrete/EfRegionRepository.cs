﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Domain.Concrete
{
    public class EfRegionRepository : EfBaseRepository, IRegionRepository, IDisposable
    {
        public IQueryable<Region> Regions => Context.Regions;

        public async Task RegionCreate(Region c)
        {
            if(c.Id != 0) return;
            Context.Regions.Add(c);
            await Context.SaveChangesAsync();
        }

        public async Task<Region> RegionGet(int id)
        {
            var q = Context.Regions.AsNoTracking()
                .Where(z => z.Id == id);
            var f = await q.AnyAsync();
            if(!f) return new Region();
            return await q.SingleAsync();
        }

        public async Task<(List<Region> items, int totalItems)> RegionGetRange(int page, int pageSize)
        {
            var q = Context.Regions.AsNoTracking();
            var totalItems = q.Count();
            if (page > 1) q = q.Skip((page - 1) * pageSize);
            if (pageSize > 0) q = q.Take(pageSize);
            var f = await q.AnyAsync();
            if (!f) return (new List<Region>(), 0);
            var list = await q.ToListAsync();
            return (list, totalItems);
        }

        public async Task RegionUpdate(Region c)
        {
            Context.Entry(c).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public async Task RegionRemove(int id)
        {
            var q = Context.Regions.Where(z => z.Id == id);
            var f = await q.AnyAsync();
            if(!f) return;
            var c = await q.SingleAsync();
            Context.Regions.Remove(c);
            await Context.SaveChangesAsync();
        }
    }
}

