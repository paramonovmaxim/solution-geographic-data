﻿using Domain.Entities;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Domain.Concrete
{
    public class EfDbContext : DbContext
    {
        public EfDbContext(DbContextOptions<EfDbContext> options)
            : base(options)
        {
        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<City> Cities { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasIndex(z => z.Name);
            modelBuilder.Entity<Region>().HasIndex(z => z.Name);
            modelBuilder.Entity<City>().HasIndex(z => z.Name);
        }
    }
}
