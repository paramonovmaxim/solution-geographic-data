﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Domain.Concrete
{
    public class EfBaseRepository
    {
        protected readonly EfDbContext Context;

        protected EfBaseRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<EfDbContext>();
            optionsBuilder.UseSqlServer(Constants.DefaultConnection);
            Context = new EfDbContext(optionsBuilder.Options);
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public void Dispose()
        {

        }
    }
}
