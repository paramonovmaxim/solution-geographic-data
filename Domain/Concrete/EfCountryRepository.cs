﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;
using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Domain.Concrete
{
    public class EfCountryRepository : EfBaseRepository, ICountryRepository, IDisposable
    {
        public IQueryable<Country> Countries => Context.Countries;

        public async Task CountryCreate(Country c)
        {
            if(c.Id != 0) return;
            Context.Countries.Add(c);
            await Context.SaveChangesAsync();
        }

        public async Task<Country> CountryGet(int id)
        {
            var q = Context.Countries.AsNoTracking()
                .Where(z => z.Id == id);
            var f = await q.AnyAsync();
            if(!f) return new Country();
            return await q.SingleAsync();
        }
        
        public async Task<(List<Country> items, int totalItems)> CountryGetRange(int page, int pageSize)
        {
            var q = Context.Countries.AsNoTracking();
            var totalItems = q.Count();
            if (page > 1) q = q.Skip((page - 1) * pageSize);
            if (pageSize > 0) q = q.Take(pageSize);
            var f = await q.AnyAsync();
            if(!f) return (new List<Country>(), 0);
            var list = await q.ToListAsync();
            return (list, totalItems);
        }

        public async Task CountryUpdate(Country c)
        {
            Context.Entry(c).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public async Task CountryRemove(int id)
        {
            var q = Context.Countries.Where(z => z.Id == id);
            var f = await q.AnyAsync();
            if(!f) return;
            var c = await q.SingleAsync();
            Context.Countries.Remove(c);
            await Context.SaveChangesAsync();
        }
    }
}
